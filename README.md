# Utils PPA

## Description
This PPA [**ppa:mikhailnov/ppa**](https://launchpad.net/~mikhailnov/+archive/ubuntu/utils/) contains different packages which the author uses for himself and his production systems and shares them with other people.

## Target distributions
Depending on package, packages are built for Ubuntu 16.04, 18.04, 18.10.
Non-binary packages may also be used on other Debian-based system (Debian, Ubuntu, Astra, Deepin).

## Adding this repository
### Add this repository to Ubuntu/Mint
```
sudo add-apt-repository ppa:mikhailnov/utils -y
sudo apt update
sudo apt dist-upgrade
```
### Add this repository to Debian/Deepin/Astra

Additionaly we set priority of the repo to avoid upgrading system's binary packages to the newer ones from the repository, but which are built for Ubuntu, not Debian. See `man apt_preferences` for details.
```
echo "deb http://ppa.launchpad.net/mikhailnov/utils/ubuntu xenial main" | sudo tee /etc/apt/sources.list.d/mikhailnov-ubuntu-utils-xenial.list
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys FE3AE55CF74041EAA3F0AD10D5B19A73A8ECB754 
echo -e "Package: * \nPin: release o=LP-PPA-mikhailnov-utils \nPin-Priority: 1" | sudo tee /etc/preferences.d/mikhailnov-ubuntu-utils-ppa
sudo apt update
```

## Packages in the repository
If the package is not in this list, than it's probably a test built or something like that.

| Source package | Description |
| --- | --- |
| [system-autoupdate](https://gitlab.com/mikhailnov/system-autoupdate) | Set of scripts and systemd units to perform automatic system updates of different Linux distributions blocking system shutdown for the time of the update. I'm the author. Recommend to use with `apt-btrfs-snapshot`. |
| policykit-1 | In Debian stable, sid and Ubuntu policykit-1 is 0.105, what is 5 years old; however, its newest version is maintained id Debian experimantal, I rebuilt it because a newer Policykit-1 is needed for system-autoupdate. Base testing did not find regressions. |
| wps-office | Repacked [official deb package](http://wps-community.org/downloads) with fixes |
| libpng12 | Legacy library needed for wps-office (security is not maintained, may cointain unclosed CVEs); rebuilt from old Ubuntu release |
| tiff3 | Legacy `libtiff3` rebuilt from old Debian LTS with **many** CVEs closed by Debian maintainers; needed for some printer drivers; security is not maintained |
| woeusb | GUI & CLI program to make a bootable USB with Windows|
| xviewer | Image viewer from Linux Mint, fast and comfortable to use! |
| [wined-advego-plagiatus](https://gitlab.com/nixtux-packaging/wined-advego-plagiatus) | Advego Plagiatus packaged for running in Wine |
| [apt-btrfs-snapshot](https://gitlab.com/nixtux-packaging/apt-btrfs-snapshot) | My fork of this Ubuntu's utility for automated BTRFS snapshots, including snapshots on every apt's action with packages. See the link for full description of changes. |
| [btrfsmaintenance](https://gitlab.com/nixtux-packaging/btrfsmaintenance/blob/master/debian/README.md) | Based on Debian's package but with some changes to auto enable & start systemd units on package installation and to make in buildable for older Debians and Ubuntus. See the link for full description of changes. |
| [dumacast](https://gitlab.com/mikhailnov/dumasnap)  | Script with GUI to automate broadcasting to a virtual webcam and microphone |
| v4l2loopback | `dkms-v4l2loopback` of the newest version (used in [Dumacast](https://gitlab.com/mikhailnov/dumasnap) |
| [telegram-desktop](https://gitlab.com/nixtux-packaging/telegram-ubuntu) | Telegram Messenger GUI open source client of a newer version. Package is rebuilt from Debian with minor changes. |
| libtgvoip | Build dependency for Telegram Desktop |
| smartmontools | smartmontools is very old in Debian, a newer version is built based on Debian's packaging; Debian bug [898121](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=898121) is fixed |
| slop | slop is in Ubuntu 18.04, but not in 16.04; Dumacast's dependency |
| rawtherapee | Rawtherapee of a newer version, rebuilt from Debian Sid |
| pulseeffects | [PulseEffects](https://github.com/wwmm/pulseeffects) program; duplicate of ppa:mikhailnov/pulseeffects. |
| calf | `calf-plugins` is of a very old version in Debian; rebuilt Deepin's package of a newer version; however, Deepin set version 0.0.90, what is incorrect, I changed it to 0.90.0 as per upstream. `calf-plugins ⩾ 0.90.0` is needed for PulseEffects; duplicate of ppa:mikhailnov/pulseeffects. |
| [dumasnap](https://gitlab.com/mikhailnov/dumasnap) | Dumasnap is in early stage of development and may be tranformed to using openSUSE's `snapper`; currently provides systemd timer to create daily BTRFS snapshots using `apt-btrfs-snapshot`. |
| hw-probe | Utility to send probes of hardware into the database [linux-hardware.org](https://linux-hardware.org); duplicate of ppa:mikhailnov/hw-probe |
| guvcview | Rebuild newer version from Ubuntu 18.04 for Ubuntu 16.04 |
| dumalogiya-fonts | Set of open fonts (`Astra PT Serif`, `XO Fonts` and some other open fonts. I use it to provide a base font set across different remote and office machines for exchanging documents with fonts, which are installed on every machine. Package needs refactoring. |
| [nastr](https://gitlab.com/mikhailnov/nastr) | My script for doing initial setup of Xubuntu installation; it's in early stage of development |
| nixtux-sysctl | My set of Linux kernel options (/etc/sysctl.d/) for using on all machines and servers. Built from `nastr` source. |
| [nixtux-rc](https://gitlab.com/mikhailnov/nixtux-rc) | This script will download gpg-signed scripts from my server and execute them; needed for remote control of machines; please **don't install nixtux-rc** unless you clearly understand what you are doing. |
| [pidgin-plugin-window-merge](https://github.com/mikhailnov/window_merge/tree/master/debian) | Plugin for Pidgin to merge many windows into one |
| purple-matrix | Just rebuilt a newer `purple-matrix` from debian Sid because the old version in Ubuntu repositories stopped working after API change |
| purple-vk-plugin | VKontakte (Вконтакте) plugin for Pidgin. Is not under development any more and works badly. |
| [ppastats](http://wpitchoune.net/ppastats/) | Generates statistics of the Launchpad PPA |
| [GIMP 2.10](https://gitlab.com/nixtux-packaging/gimp-ubuntu) | GIMP 2.10 for Ubuntu >=18.04 with **patches** (see the link) |
| [PulseAudio 12](https://gitlab.com/nixtux-packaging/pulseaudio-ubuntu) | [Bugs](https://github.com/wwmm/pulseeffects/issues/99)  of module-switch-on-connect were fixed in PulseAudio 12. Ubuntu's patches ported from 18.10 to 18.04. Both `pulseaudio-module-gconf` and `pulseaudio-module-gsettings` are built. |
| [Wirec](https://gitlab.com/mikhailnov/wirec)  | Wirec script |
| [converts_print](https://gitlab.com/mikhailnov/converts_print)  | converts_print script |
| [kazam](https://gitlab.com/nixtux-packaging/kazam-ubuntu/) | Kazam screen recorder with my patch, which fixes loading translations (localizations) of its GUI |

